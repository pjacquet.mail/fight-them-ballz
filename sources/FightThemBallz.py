import pygame
import random
import math

pygame.init()

# Constantes pour les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
JAUNE = (255, 255, 0)
BLEU = (0, 0, 255)

# Constantes pour les pièces
nombre_pieces = 15
pieces = []
piece_taille = 25
piece_valeur = 1
# Récupérer la résolution de l'écran
largeur, hauteur = pygame.display.Info().current_w, pygame.display.Info().current_h

# Initialiser la fenêtre de jeu avec la résolution de l'écran
fenetre = pygame.display.set_mode((largeur, hauteur))
pygame.display.set_caption("Ramasser des pièces")

# Chargez l'image de fond et redimensionnez-la
texture_arriere_plan = pygame.image.load("background.jpg")
texture_arriere_plan = pygame.transform.scale(texture_arriere_plan, (largeur, hauteur))
# Chargez l'image de pièce et redimensionnez-l
texture_piece = pygame.image.load("coin.png").convert_alpha()
texture_piece = pygame.transform.scale(texture_piece, (piece_taille, piece_taille))
# Constantes pour le joueur
joueur_taille = 30
vitesse_joueur = 0.7  # Vitesse de déplacement initiale du joueur
friction = 0.1  # Coefficient de friction pour simuler l'inertie



# Constantes pour les canons
nombre_canons = 10
canons = []
canon_taille = 25
vitesse_canon_base = 5
vitesse_canon = vitesse_canon_base

texture_canon = pygame.image.load("ennemi.png").convert_alpha()
texture_canon = pygame.transform.scale(texture_canon, (canon_taille, canon_taille))

# Constantes pour les plateformes
nombre_plateformes = 10
plateformes = []
plateforme_taille = (150, 25)


# Charger la texture "barrier.png" avec transparence
texture_plateforme = pygame.image.load("barrier.png").convert_alpha()
texture_plateforme = pygame.transform.scale(texture_plateforme, plateforme_taille)

# Temps par niveau
temps_par_niveau = 55 * 1000  # 45 secondes en millisecondes

# Compteurs
score = 0
niveau = 1
temps_debut_niveau = pygame.time.get_ticks()

texture_joueur = pygame.image.load("player.png").convert_alpha()
texture_joueur = pygame.transform.scale(texture_joueur, (joueur_taille, joueur_taille))

# Fonction pour dessiner le joueur
def dessiner_joueur(x, y):
    fenetre.blit(texture_joueur, (x, y))

# Fonction pour dessiner les pièces avec la texture
def dessiner_pieces():
    for piece in pieces:
        fenetre.blit(texture_piece, (piece[0], piece[1]))
# Fonction pour dessiner les canons avec la texture
def dessiner_canons():
    for canon in canons:
        fenetre.blit(texture_canon, (canon[0], canon[1]))

# Fonction pour dessiner les plateformes
def dessiner_plateformes():
    for plateforme in plateformes:
        fenetre.blit(texture_plateforme, (plateforme[0], plateforme[1]))


# Fonction pour créer les pièces
def creer_pieces():
    for _ in range(nombre_pieces):
        x = random.randint(0, largeur - piece_taille)
        y = random.randint(0, hauteur - piece_taille)
        pieces.append([x, y])

# Fonction pour créer les canons
def creer_canons():
    for _ in range(nombre_canons):
        x = random.randint(0, largeur - canon_taille)
        y = random.randint(0, hauteur - canon_taille)
        canons.append([x, y, random.uniform(0, 360)])

# Fonction pour créer les plateformes
def creer_plateformes():
    for _ in range(nombre_plateformes):
        orientation = random.choice(["horizontale", "verticale"])
        if orientation == "horizontale":
            x = random.randint(0, largeur - plateforme_taille[0])
            y = random.randint(0, hauteur - plateforme_taille[1])
            plateformes.append([x, y, plateforme_taille, "bleue"])
        else:
            x = random.randint(0, largeur - plateforme_taille[1])
            y = random.randint(0, hauteur - plateforme_taille[0])
            plateformes.append([x, y, (plateforme_taille[1], plateforme_taille[0]), "verte"])

# Déplacer les canons
def deplacer_canons():
    for canon in canons:
        angle = math.radians(canon[2])
        canon[0] += vitesse_canon * math.cos(angle)
        canon[1] += vitesse_canon * math.sin(angle)
        if canon[0] < 0 or canon[0] > largeur - canon_taille:
            canon[2] = 180 - canon[2]
        if canon[1] < 0 or canon[1] > hauteur - canon_taille:
            canon[2] = -canon[2]

# Vérifier si le joueur a ramassé une pièce
def verifier_ramassage_piece(joueur_x, joueur_y):
    global score
    joueur_rect = pygame.Rect(joueur_x, joueur_y, joueur_taille, joueur_taille)
    for piece in pieces:
        piece_rect = pygame.Rect(piece[0], piece[1], piece_taille, piece_taille)
        if joueur_rect.colliderect(piece_rect):
            pieces.remove(piece)
            score += piece_valeur

# Vérifier si le joueur a été touché par un canon
def verifier_collision_joueur_canon(joueur_x, joueur_y):
    joueur_rect = pygame.Rect(joueur_x, joueur_y, joueur_taille, joueur_taille)
    for canon in canons:
        canon_rect = pygame.Rect(canon[0], canon[1], canon_taille, canon_taille)
        if joueur_rect.colliderect(canon_rect):
            return True
    return False

# Vérifier si toutes les pièces ont été ramassées
def verifier_fin_niveau():
    return len(pieces) == 0

# Vérifier si un canon entre en collision avec une plateforme
def verifier_collision_canon_plateforme():
    for canon in canons:
        canon_rect = pygame.Rect(canon[0], canon[1], canon_taille, canon_taille)
        for plateforme in plateformes:
            plateforme_rect = pygame.Rect(plateforme[0], plateforme[1], plateforme[2][0], plateforme[2][1])
            if canon_rect.colliderect(plateforme_rect):
                # Inverser la direction du mouvement du canon
                canon[2] += 180
                break

# Fonction pour afficher le score
def afficher_score():
    font = pygame.font.Font(None, 36)
    text = font.render("Score: " + str(score), True, NOIR)
    fenetre.blit(text, (20, 50))

# Fonction pour afficher le temps restant
def afficher_temps_restant():
    temps_actuel = pygame.time.get_ticks()
    temps_ecoule = temps_actuel - temps_debut_niveau
    temps_restant = max(temps_par_niveau - temps_ecoule, 0)
    temps_restant_secondes = temps_restant // 1000
    font = pygame.font.Font(None, 36)
    text = font.render("Temps restant: " + str(temps_restant_secondes) + "s", True, NOIR)
    fenetre.blit(text, (largeur - 250, 25))

# Fonction pour afficher le niveau
def afficher_niveau():
    font = pygame.font.Font(None, 36)
    text = font.render("Niveau: " + str(niveau), True, NOIR)
    fenetre.blit(text, (20, 30))

# Fonction pour afficher le message "Appuyez sur ESPACE pour passer"
def afficher_message_passer():
    font = pygame.font.Font(None, 36)
    text = font.render("Appuyez sur ESPACE pour passer", True, NOIR)
    fenetre.blit(text, (largeur // 2 - 200, hauteur // 2))

# Fonction pour attendre que la barre d'espace soit enfoncée
def attendre_espace():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                return

# Fonction pour réinitialiser le jeu
def reinitialiser_jeu():
    global score, niveau, temps_debut_niveau
    score = 0
    niveau = 1
    creer_pieces()  # Réinitialiser les pièces
    temps_debut_niveau = pygame.time.get_ticks()  # Réinitialiser le temps de début du niveau

# Initialisation du joueur
joueur_x = largeur // 2 - joueur_taille // 2
joueur_y = hauteur // 2 - joueur_taille // 2
velocite_joueur_x = 0
velocite_joueur_y = 0

creer_pieces()
creer_canons()
creer_plateformes()

# Initialisation du chronomètre au démarrage
temps_debut_chrono = pygame.time.get_ticks()

# Boucle principale
running = True
clock = pygame.time.Clock()

while running:
    fenetre.blit(texture_arriere_plan, (0, 0))  # Dessiner le fond d'écran

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Vérifier si le chronomètre de 10 secondes est écoulé
    temps_actuel = pygame.time.get_ticks()
    if temps_actuel - temps_debut_chrono < 10000:
        temps_restant_secondes = (10000 - (temps_actuel - temps_debut_chrono)) // 1000
        font = pygame.font.Font(None, 36)
        text = font.render("Démarrage dans: " + str(temps_restant_secondes) + "s", True, NOIR)
        fenetre.blit(text, (largeur // 2 - 100, hauteur // 2))
        pygame.display.update()
        continue  # Passer à l'itération suivante de la boucle sans exécuter le reste du code

    touches = pygame.key.get_pressed()

    # Modifier la vélocité du joueur en réponse aux entrées du joueur
    if touches[pygame.K_LEFT]:
        velocite_joueur_x -= vitesse_joueur
    elif touches[pygame.K_RIGHT]:
        velocite_joueur_x += vitesse_joueur

    if touches[pygame.K_UP]:
        velocite_joueur_y -= vitesse_joueur
    elif touches[pygame.K_DOWN]:
        velocite_joueur_y += vitesse_joueur

    # Ajouter des frottements pour simuler l'inertie
    velocite_joueur_x *= (1 - friction)
    velocite_joueur_y *= (1 - friction)

    # Mettre à jour les coordonnées du joueur en fonction de la vélocité
    joueur_x += velocite_joueur_x
    joueur_y += velocite_joueur_y

    deplacer_canons()
    verifier_collision_canon_plateforme()
    dessiner_canons()
    dessiner_joueur(joueur_x, joueur_y)
    dessiner_pieces()
    dessiner_plateformes()
    verifier_ramassage_piece(joueur_x, joueur_y)

    if verifier_collision_joueur_canon(joueur_x, joueur_y):
        running = False

    if verifier_fin_niveau():
        score += 100
        niveau += 1
        pieces.clear()
        creer_pieces()
        temps_debut_niveau = pygame.time.get_ticks()

    afficher_score()
    afficher_temps_restant()
    afficher_niveau()

    # Si le temps est écoulé, afficher le message et attendre que l'utilisateur appuie sur espace
    temps_actuel = pygame.time.get_ticks()
    if temps_actuel - temps_debut_niveau >= temps_par_niveau:
        afficher_message_passer()
        attendre_espace()  # Attendre que l'utilisateur appuie sur espace pour passer au niveau suivant ou relancer le jeu
        reinitialiser_jeu()  # Réinitialiser le jeu si l'utilisateur appuie sur espace



    pygame.display.update()
    clock.tick(60)

# Afficher le nombre de points du joueur
print("Votre score est de :", score)

pygame.quit()
